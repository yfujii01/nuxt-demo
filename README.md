# Nuxt demo

## ドキュメント

1. [デバッグ設定](./doc/デバッグ設定.md)
1. [デプロイ環境](./doc/デプロイ環境.md)

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
